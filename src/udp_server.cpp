#include "udp_server.h"

using namespace std;

UDPServer::UDPServer(int port)
{
    udp_manager_.Create();
    udp_manager_.Bind(port);
    udp_manager_.SetNonBlocking(true);
}

UDPServer::~UDPServer()
{
    udp_manager_.Close();
}

void UDPServer::receive(char* messages, int length)
{
    if(int len = udp_manager_.Receive(messages,length))
    {
        for(int i=0; i<len;i++)
        {
            float x = messages[i];
            printf("%.1f ", x);
        }
        cout<<endl;
    }
    
}