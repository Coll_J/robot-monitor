#pragma once
#include <bits/stdc++.h>
#include "ofxNetwork.h"

class UDPServer
{
private:
    ofxUDPManager udp_manager_;

    

public:

    UDPServer(int port);
    ~UDPServer();

    void receive(char* messages, int length);
};